package mycompany;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "mycompany.repository")
@ComponentScan (basePackages = "mycompany.service")
public class Config {
}
