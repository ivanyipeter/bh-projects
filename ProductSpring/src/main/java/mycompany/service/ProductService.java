package mycompany.service;

import mycompany.dto.ProductDTO;
import mycompany.entity.Product;
import mycompany.repository.ProductRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.print.attribute.standard.PDLOverrideSupported;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {


    private ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<ProductDTO> getAllProducts() {
        List<ProductDTO> productDTOS = new ArrayList<>();
        List<Product> products = productRepository.findAll();
        products.forEach(item -> {
            ProductDTO dto = new ProductDTO();
            BeanUtils.copyProperties(item, dto);
            productDTOS.add(dto);

        });
        return productDTOS;
    }

    public List<ProductDTO> getAllProductsAscendingPrice() {
        List<ProductDTO> productDTOS = new ArrayList<>();
        List<Product> products = productRepository.ascendingPrice();
        products.forEach(item -> {
            ProductDTO dto = new ProductDTO();
            BeanUtils.copyProperties(item, dto);
            productDTOS.add(dto);

        });
        return productDTOS;
    }

    public void deleteEntityById(Long id) {
        productRepository.deleteById(id);
    }

    public ProductDTO getProductById(Long id) {
        Product product = productRepository.getOne(id);
        ProductDTO productDTO = new ProductDTO();
        BeanUtils.copyProperties(product, productDTO);
        return productDTO;
    }

    public void saveItem(Product product) {
        productRepository.save(product);
    }

    public List<ProductDTO> getProductByName(String name) {
        List<ProductDTO> productDTOS = new ArrayList<>();
        List<Product> products = productRepository.findAllByName(name);
        products.forEach(item -> {
            ProductDTO dto = new ProductDTO();
            BeanUtils.copyProperties(item, dto);
            productDTOS.add(dto);

        });
        return productDTOS;
    }

}
