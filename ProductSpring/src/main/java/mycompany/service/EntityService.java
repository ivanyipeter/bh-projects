package mycompany.service;

import mycompany.entity.Product;

public class EntityService {

    public static Product createEntity(Long id, String name, int price) {
        Product product = new Product();
        product.setPrice(price);
        product.setName(name);
        product.setId(id);
        return product;
    }
}
