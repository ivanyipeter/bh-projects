package mycompany.repository;

import mycompany.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query("SELECT a FROM Product a ORDER BY a.price ASC")
    List<Product> ascendingPrice();


    List<Product> findAllByName(String name);

}
