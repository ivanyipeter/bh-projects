package mycompany.rest;


import mycompany.dto.ProductResponse;
import mycompany.service.EntityService;
import mycompany.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("product")
public class ProductRestController {

    @Autowired
    private ProductService productService;

    @RequestMapping(method = RequestMethod.POST, path = "listall")
    public ResponseEntity getAllProducts() {
        ProductResponse productResponse = new ProductResponse();
        productResponse.setProductDTOS(productService.getAllProducts());
        return ResponseEntity.ok(productResponse);
    }

    @RequestMapping(method = RequestMethod.POST, path = "asclist")
    public ResponseEntity getAllProductsAscendingPrice() {
        ProductResponse productResponse = new ProductResponse();
        productResponse.setProductDTOS(productService.getAllProductsAscendingPrice());
        return ResponseEntity.ok(productResponse);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "delete/{id}")
    public void deleteById(@PathVariable(value = "id") Long id) {
        productService.deleteEntityById(id);
    }

    @RequestMapping(method = RequestMethod.POST, path = "getbyid/{id}")
    public ResponseEntity getById(@PathVariable(value = "id") Long id) {
        ProductResponse productResponse = new ProductResponse();
        productResponse.setItem(productService.getProductById(id));
        return ResponseEntity.ok(productResponse);
    }

    @RequestMapping(method = RequestMethod.POST, path = "getbyname/{name}")
    public ResponseEntity getByName(@PathVariable(value = "name") String name) {
        ProductResponse productResponse = new ProductResponse();
        productResponse.setProductDTOS(productService.getProductByName(name));
        return ResponseEntity.ok(productResponse);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "add/{id}/{name}/{price}")
    public void saveProduct(@PathVariable(value = "id") Long id,
                                  @PathVariable(value = "name") String name,
                                  @PathVariable(value = "price") int price) {
        productService.saveItem(EntityService.createEntity(id,name,price));
    }


}
