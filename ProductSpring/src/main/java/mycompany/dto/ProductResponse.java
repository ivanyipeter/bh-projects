package mycompany.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ProductResponse {

    @JsonProperty("products")
    List<ProductDTO> productDTOS = new ArrayList<>();

    public List<ProductDTO> getProductDTOS() {
        return productDTOS;
    }

    public void setProductDTOS(List<ProductDTO> productDTOS) {
        this.productDTOS = productDTOS;
    }

    public void setItem (ProductDTO productDTO){
        productDTOS.add(productDTO);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProductResponse)) return false;
        ProductResponse that = (ProductResponse) o;
        return Objects.equals(productDTOS, that.productDTOS);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productDTOS);
    }

}
