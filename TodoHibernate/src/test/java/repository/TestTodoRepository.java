/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import com.mycompany.todohibernate.entity.Todo;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.PersistenceException;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author ivany
 */
//@ExtendWith(MockitoExtension.class)
public class TestTodoRepository {
//
//    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
//    TodoRepository todoRepository;
//
//    List<Todo> filterTodoList;
//
//    Todo testTodo;
//
//    @BeforeEach
//    public void setupFilteredTodoList() {
//        filterTodoList = new ArrayList<>();
//        Todo todo1 = new Todo();
//        todo1.setDescription("Descr");
//        todo1.setSummary("Sum");
//        Todo todo2 = new Todo();
//        todo2.setDescription("desc2");
//        todo2.setSummary("sum2");
//        filterTodoList.add(todo1);
//        filterTodoList.add(todo2);
//        
//    }
//
//    @BeforeEach
//    public void setupTodo() {
//        testTodo = new Todo();
//        testTodo.setDescription("KivaloLeiras");
//        testTodo.setSummary("Összegzés");
//    }
//
//    @Test
//    public void testTodoRepo() {
//        assertAll("TestTodoRepository",
//                () -> {
//                    when(todoRepository.searchTodo("Sum", "Descr", null)).thenReturn(filterTodoList);
//
//                    assertAll("TestReturnedEntities",
//                            () -> {
//                                assertEquals(2, todoRepository.searchTodo("Sum", "Descr", null).size());
//                            },
//                            () -> {
//                                assertEquals(2, todoRepository.searchTodo("Sum", "Descr", null)
//                                        .stream().filter(todo -> "Descr"
//                                        .equals(todo.getDescription()) && "Sum"
//                                        .equals(todo.getSummary())).count());
//                            });
//                },
//                ()-> {
//                    when(todoRepository.getEm().find(Todo.class, 5L)).thenReturn(testTodo);
//                    
//                    assertTrue("KivaloLeiras"
//                            .equals(todoRepository.getEm().find(Todo.class, 5L).getDescription()));
//                },
//                ()-> {
//                    when(todoRepository.getEm().find(Todo.class, -1L)).thenThrow(PersistenceException.class);
//                    
//                }
//                );
//
//    }

}
