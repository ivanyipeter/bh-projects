<%-- 
    Document   : createTodo
    Created on : 2019.06.07., 19:49:09
    Author     : ivany
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style><%@include file="settings.css"%></style>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>JSP Page</title>

    </head>
    <body>
       
       <form action="CreateUser" align="center">
            <input type="submit" value="Create User" />
        </form>
         <form action="TodoServlet" align="center">
            <input type="submit" value="Create Todo" />
        </form>
        <form action="TodoSearch" align="center">
            <input type="submit" value="Search Todo" />
        </form>
        <br>
        
        <form method = "post" align="center">  
             <h3> Create TODO </h3>
        <br>
            <select name="userName">
                <option selected>Choose User Name</option>
                <c:forEach items="${userList}" var="user">
                    <option value="${user.name}">${user.name}</option>
                </c:forEach>
            </select>
            Summary: <input type="text" name="todoSummary"/>
            Description: <input type="text" name="todoDescription"/>
            <input type="submit" name="search" value="createTodo" />
        </form>
        <table class="table">
            <tr>
                <th scope="col">id</th>
                <th scope="col">Summary</th>
                <th scope="col">Description</th>
                <th scope="col">Name</th>
            </tr>
            <c:forEach items="${todoList}" var="Todo"> 
                <tr>
                    <td>${Todo.id}</td>
                    <td>${Todo.summary}</td>
                    <td>${Todo.description}</td>
                    <td>${Todo.user.name}</td>
                </tr>
            </c:forEach>
        </table>
        <table class="table">
            <tr>
                <th scope="col">id</th>
                <th scope="col">Name</th>
                
            </tr>
            <c:forEach items="${userList}" var="User"> 
                <tr>
                    <td>${User.id}</td>
                    <td>${User.name}</td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
