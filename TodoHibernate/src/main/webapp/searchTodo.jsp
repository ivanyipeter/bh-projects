<%-- 
    Document   : TodoSearch
    Created on : 2019.06.10., 13:20:05
    Author     : ivany
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <style><%@include file="settings.css"%></style>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="CreateUser" align="center">
            <input type="submit" value="Create User" />
        </form>
        <form action="TodoServlet" align="center">
            <input type="submit" value="Create Todo" />
        </form>
        <form action="TodoSearch" align="center">
            <input type="submit" value="Search Todo" />
        </form>

        <br>
        <form method = "post" align="center">  
            <h3> Search TODO </h3>
            <br>
            User name: <input type="text" name="user"/>
            Summary: <input type="text" name="sum"/>
            Description: <input type="text" name="desc"/>
            <input type="submit" name="search" value="search" />
        </form>


        <table class="table" align="center">
            <tr>
                <th scope="col">id</th>
                <th scope="col">Summary</th>
                <th scope="col">Description</th>
                <th scope="col">Name</th>
            </tr>
            <c:forEach items="${searchByUserName}" var="Todo"> 
                <tr>
                    <td>${Todo.id}</td>
                    <td>${Todo.summary}</td>
                    <td>${Todo.description}</td>
                    <td>${Todo.user.name}</td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
