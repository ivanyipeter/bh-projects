/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.todohibernate.repository;

import com.mycompany.todohibernate.entity.User;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

/**
 *
 * @author ivany
 */
@Stateless
public class UserRepository {

    @PersistenceContext(name = "com.mycompany_TodoHibernate_war_1.0-SNAPSHOTPU")
    EntityManager em;

    public  List<User> getUsers() {
        Query q = em.createQuery("Select g from User g", User.class);
        return q.getResultList();
    }

    @Transactional
    public void addUser(User user) {
        em.persist(user);
    }
    
     public List<User> searchByUsername(String username) {
        return em.createQuery("SELECT c FROM User c WHERE c.name = :name")
                .setParameter("name", username)
                .getResultList();
    }

}
