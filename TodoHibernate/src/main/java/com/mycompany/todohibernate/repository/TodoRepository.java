/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.todohibernate.repository;

import com.mycompany.todohibernate.entity.Todo;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

/**
 *
 * @author ivany
 */
@Stateless
public class TodoRepository {

    @PersistenceContext(name = "com.mycompany_TodoHibernate_war_1.0-SNAPSHOTPU")
    EntityManager em;

    public EntityManager getEm() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }
    
    public List<Todo> getTodos() {
        Query q = em.createQuery("Select g from Todo g", Todo.class);
        return q.getResultList();
    }

    @Transactional
    public void addTodo(Todo todo) {
        em.persist(todo);
    }

    public List<Todo> searchTodo(String sum, String descr, String name) {
        Query q1 = em.createQuery("select c FROM Todo c WHERE c.summary LIKE :summary AND c.description LIKE :description AND user.name LIKE :username", Todo.class);
        q1.setParameter("summary", "%" + sum + "%");
        q1.setParameter("description","%"+ descr+"%");
        q1.setParameter("username","%"+ name+ "%");
        return q1.getResultList();
    }
}
