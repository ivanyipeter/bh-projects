/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.todohibernate.service;

import com.mycompany.todohibernate.entity.Todo;
import com.mycompany.todohibernate.entity.User;

/**
 *
 * @author ivany
 */
public class TodoService {
    public static Todo createTodo (String desc, String sum, User user){
        Todo todo = new Todo();
        todo.setDescription(desc);
        todo.setSummary(sum);
        todo.setUser(user);
        return todo;
    }
}
