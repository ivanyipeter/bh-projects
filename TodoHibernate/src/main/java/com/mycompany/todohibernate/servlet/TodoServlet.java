/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.todohibernate.servlet;

import com.mycompany.todohibernate.entity.Todo;
import com.mycompany.todohibernate.entity.User;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mycompany.todohibernate.repository.TodoRepository;
import com.mycompany.todohibernate.repository.UserRepository;
import com.mycompany.todohibernate.service.TodoService;
import com.mycompany.todohibernate.service.UserService;
import javax.inject.Inject;

/**
 *
 * @author ivany
 */
public class TodoServlet extends HttpServlet {

    @Inject
    private TodoRepository todoRepository;
    @Inject
    private UserRepository userRepository;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setAttribute("todoList", todoRepository.getTodos());
        request.setAttribute("userList", userRepository.getUsers());
        request.getRequestDispatcher("/createTodo.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String desc = request.getParameter("todoDescription");
        String sum = request.getParameter("todoSummary");
        String userName = request.getParameter("userName");

        Todo todo = new Todo();
        todo.setDescription(desc);
        todo.setSummary(sum);

        List<User> searchByName = userRepository.searchByUsername(userName);

        if (searchByName.isEmpty()) {
            User user = UserService.userFactory(userName);
            TodoService.createTodo(desc, sum, user);
            userRepository.addUser(user);
        } else {
            todo.setUser(searchByName.get(0));
        }
        todoRepository.addTodo(todo);

        response.sendRedirect(this.getServletName());

    }
}
