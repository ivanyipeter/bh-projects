/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.todohibernate.servlet;

import com.mycompany.todohibernate.entity.User;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mycompany.todohibernate.repository.UserRepository;
import com.mycompany.todohibernate.service.UserService;
import javax.inject.Inject;

/**
 *
 * @author ivany
 */
@WebServlet(name = "CreateUser", urlPatterns = {"/CreateUser"})
public class CreateUserServlet extends HttpServlet {

    @Inject
    private UserRepository userRepository;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setAttribute("userList", userRepository.getUsers());
        request.getRequestDispatcher("/createUser.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String userName = request.getParameter("userName");
        List<User> searchByName = userRepository.searchByUsername(userName);

        if (searchByName.isEmpty()) {
            userRepository.addUser(UserService.userFactory(userName));
        }
        response.sendRedirect(this.getServletName());
    }
}
