/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.todohibernate.servlet;

import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mycompany.todohibernate.repository.TodoRepository;
import com.mycompany.todohibernate.repository.UserRepository;
import javax.inject.Inject;

/**
 *
 * @author ivany
 */
@WebServlet(name = "TodoSearch", urlPatterns = {"/TodoSearch"})
public class SearchTodoServlet extends HttpServlet {

    @Inject
    private TodoRepository todoRepository;
    @Inject
    private UserRepository userRepository;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getRequestDispatcher("searchTodo.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name = request.getParameter("user");
        String sum = request.getParameter("sum");
        String desc = request.getParameter("desc");

        request.setAttribute("searchByUserName", todoRepository.searchTodo(sum, desc, name));
        request.getRequestDispatcher("searchTodo.jsp").forward(request, response);

    }
}
