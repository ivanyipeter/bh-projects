/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package product;

import barCode.BarCode;
import java.io.Serializable;
import productParameter.BeautyProductsParameter;
import productType.ProductType;

/**
 *
 * @author ivany
 */
public class BeautyProduct extends Product implements BeautyProductsParameter, Serializable {

    public BeautyProduct(BarCode barCode, ProductType productType, String manufacturer, String price) {
        super(barCode, productType, manufacturer, price);
    }

    @Override
    public void setWeight() {
    }

    @Override
    public void example() {
    }
    
}
