/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package product;

import barCode.BarCode;
import java.io.Serializable;
import productParameter.EntertainmentException;
import productParameter.EntertainmentProductsParameters;
import productType.ProductType;

/**
 *
 * @author ivany
 */
public class EntertainmentProduct extends Product implements  EntertainmentProductsParameters, Serializable{
    
    public static int counter;

    public EntertainmentProduct(BarCode barCode, ProductType productType, String manufacturer, String price) {
        super(barCode, productType, manufacturer, price);
    }

    @Override
    public void canTurnOn() {
        System.out.println("Turned on");
        try {
            counter++;
            if (counter>5) throw new EntertainmentException();
        } catch (EntertainmentException ex) {
            System.out.println(ex);;
        }
    }

    @Override
    public void example() {
    }
    
}
