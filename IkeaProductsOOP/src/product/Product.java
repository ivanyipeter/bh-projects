/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package product;

import barCode.BarCode;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import productType.ProductType;

/**
 *
 * @author ivany
 */
public abstract class Product implements Serializable, InterfaceExample {

    private BarCode barCode;
    private ProductType productType;
    private String manufacturer;
    private String price;
    private LocalDate date;

    public Product(BarCode barCode, ProductType productType, String manufacturer, String price) {
        this.barCode = barCode;
        this.productType = productType;
        this.manufacturer = manufacturer;
        this.price = price;
        this.date = LocalDate.now();
    }

    public BarCode getBarCode() {
        return barCode;
    }

    public void setBarCode(BarCode barCode) {
        this.barCode = barCode;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.barCode);
        hash = 29 * hash + Objects.hashCode(this.productType);
        hash = 29 * hash + Objects.hashCode(this.manufacturer);
        hash = 29 * hash + Objects.hashCode(this.price);
        hash = 29 * hash + Objects.hashCode(this.date);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Product other = (Product) obj;
        if (!Objects.equals(this.barCode, other.barCode)) {
            return false;
        }
        if (this.productType != other.productType) {
            return false;
        }
        if (!Objects.equals(this.manufacturer, other.manufacturer)) {
            return false;
        }
        if (!Objects.equals(this.price, other.price)) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Product{" + "barCode=" + barCode + ", productType=" + productType + ", manufacturer=" + manufacturer + ", price=" + price + ", date=" + date + '}';
    }

    
}