/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package product;

import barCode.BarCode;
import java.io.Serializable;
import productParameter.KitchenProductsParameters;
import productType.ProductType;

/**
 *
 * @author ivany
 */
public class KitchenProduct extends Product implements KitchenProductsParameters, Serializable {

    public KitchenProduct(BarCode barCode, ProductType productType, String manufacturer, String price) {
        super(barCode, productType, manufacturer, price);
    }

    @Override
    public void canLift() {
    }

    @Override
    public void canTurnOn() {
    }

    @Override
    public void example() {
    }

}
