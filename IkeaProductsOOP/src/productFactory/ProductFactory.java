/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productFactory;

import barCode.BarCode;
import product.BeautyProduct;
import product.EntertainmentProduct;
import product.KitchenProduct;
import product.Product;
import productType.ProductType;

/**
 *
 * @author ivany
 */
public class ProductFactory {

    public static Product create(String barCode, String productType, String manufacturer, String price, String type) {

        int bc = Integer.parseInt(barCode);

        switch (type) {
            case "kitchen":
                return new KitchenProduct(new BarCode(bc), checkProductType(productType), manufacturer, price);
            case "ent":
                return new EntertainmentProduct(new BarCode(bc), checkProductType(productType), manufacturer, price);
            case "beauty":
                return new BeautyProduct(new BarCode(bc), checkProductType(productType), manufacturer, price);
        }

        throw new IllegalArgumentException("");
    }

    public static ProductType checkProductType(String s) {
        switch (s) {
            case "cheap":
                return ProductType.CHEAP;
            case "average":
                return ProductType.AVERAGE;
            case "luxury":
                return ProductType.LUXURY;
            case "popular":
                return ProductType.POPULAR;
        }
        return null;
    }
}
