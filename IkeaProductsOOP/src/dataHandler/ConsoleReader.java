/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataHandler;

import barCode.BarCode;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import product.Product;
import productFactory.ProductFactory;
import repository.Repository;

public class ConsoleReader {

    private Repository repository = new Repository();

    public void command() {
        printInfo();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            String line;
            int counter = 1;
            while ((line = br.readLine()) != null && !"exit".equals(line) && counter != 10) {
                parser(line);
                counter++;
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public void parser(String line) {
        String[] l = line.split(" ");

        if ((l[0]).equals("add")) {
            Product product = ProductFactory.create(l[1], l[2], l[3], l[4], l[5]);
            repository.add(product);
        }
        if ((l[0]).equals("save")) {
            serializeOut();
            new Report(repository).numberOfProducts();
        }
        if ((l[0]).equals("remove")) {
            removeBarcode(l[1]);
            new Report(repository).numberOfProducts();
        }
        if ((l[0]).equals("load")) {
            serializeIn();
            new Report(repository).numberOfProducts();
            
        }
    }

    public void removeBarcode(String barCode) {
        int bc = Integer.parseInt(barCode);
        List<Product> list = repository.getList();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getBarCode().getSerialNumber() == bc) {
                list.remove(i);
            }
        }
    }

    public void printInfo() {
        System.out.println("add vonalkód típus(cheap, average, luxury, popular) gyártó ár speciális típus(kitchen, ent, beauty)");
        System.out.println("remove vonalkód");
        System.out.println("save/load");
    }

    public void serializeOut() {
        try (ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream("repository.ser"))) {
            o.writeObject(repository);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Serializálás sikeres");
    }

    public void serializeIn() {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("repository.ser"))) {
            repository = (Repository) ois.readObject();
        } catch (IOException ex) {
            Logger.getLogger(ConsoleReader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ConsoleReader.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
