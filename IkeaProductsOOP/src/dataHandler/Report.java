/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataHandler;

import java.util.List;
import product.BeautyProduct;
import product.EntertainmentProduct;
import product.KitchenProduct;
import product.Product;
import repository.Repository;

/**
 *
 * @author ivany
 */
public class Report {

    private final Repository repository;

    public Report(Repository repository) {
        this.repository = repository;
    }

    public void numberOfProducts() {
        int kp = 0;
        int bp = 0;
        int ep = 0;
        List<Product> list = repository.getList();
        for (Product list1 : list) {
            if (list1 instanceof KitchenProduct) {
                kp++;
            } else if (list1 instanceof EntertainmentProduct) {
                ep++;
            } else if (list1 instanceof BeautyProduct) {
                bp++;
            }
        }
        System.out.println("Number of Kitchen products " + kp);
        System.out.println("Number of Entertainment products:" + ep);
        System.out.println("Number of Beauty products" + bp);
    }
}
