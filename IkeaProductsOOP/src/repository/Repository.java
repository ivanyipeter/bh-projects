/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import product.Product;

/**
 *
 * @author ivany
 */
public class Repository implements Serializable {

    private List<Product> list = new ArrayList<>();

    public void add(Product p) {
        list.add(p);
    }

    public void remove(Product p) {
        list.remove(p);
    }

    public List<Product> getList() {
        return list;
    }

    public int size() {
        return list.size();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 43 * hash + Objects.hashCode(this.list);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Repository other = (Repository) obj;
        if (!Objects.equals(this.list, other.list)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Repository{" + "list=" + list + '}';
    }

}
