/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package barCode;

import java.util.Comparator;

/**
 *
 * @author ivany
 */
public class BarCodeComparator implements Comparator<BarCode> {

    @Override
    public int compare(BarCode o1, BarCode o2) {
        return o1.getSerialNumber()-o2.getSerialNumber();
    }
    
}
