/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package barCode;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author ivany
 */
public class BarCode implements Serializable {

    private BarCodeType barCodeType;
    private int serialNumber;
    private static int counter;

    public BarCode(int serialNumber) {
        counter++;
        if (counter % 2 == 0) {
            this.barCodeType = BarCodeType.DIGITAL;
        } else this.barCodeType = BarCodeType.PRINT;

        this.serialNumber = serialNumber;
    }

    public BarCodeType getBarCodeType() {
        return barCodeType;
    }

    public void setBarCodeType(BarCodeType barCodeType) {
        this.barCodeType = barCodeType;
    }

    public int getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }

    public static int getCounter() {
        return counter;
    }

    public static void setCounter(int counter) {
        BarCode.counter = counter;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 73 * hash + Objects.hashCode(this.barCodeType);
        hash = 73 * hash + this.serialNumber;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BarCode other = (BarCode) obj;
        if (this.barCodeType != other.barCodeType) {
            return false;
        }
        if (this.serialNumber != other.serialNumber) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BarCode{" + "barCodeType=" + barCodeType + ", serialNumber=" + serialNumber + '}';
    }

}
